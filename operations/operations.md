# Operations Management

- [Operations Management](#operations-management)
- [Agenda](#agenda)
- [Why operations management?](#why-operations-management)
- [Role of Operations Management](#role-of-operations-management)
- [Alternative Configurations in Operations Management](#alternative-configurations-in-operations-management)
  - [Streamlined/Continous Flow Systems](#streamlinedcontinous-flow-systems)
    - [Issues](#issues)
  - [Intermittent Flow Systems](#intermittent-flow-systems)
    - [Features](#features)
    - [Sources of Problems](#sources-of-problems)
    - [Role of Operations Management](#role-of-operations-management-1)
  - [Jumbled Flow Systems](#jumbled-flow-systems)
    - [Issues to address](#issues-to-address)
    - [How to handle this?](#how-to-handle-this)
  - [Observations](#observations)
    - [Volume Variety](#volume-variety)
    - [Complexity](#complexity)
- [Perfomance metrics](#perfomance-metrics)
  - [Performance Metrics for Operations](#performance-metrics-for-operations)
    - [Growing customer expectations](#growing-customer-expectations)
    - [Priorities for Operations Today](#priorities-for-operations-today)
    - [Order winning and Order qualifying attributes](#order-winning-and-order-qualifying-attributes)
- [Analyzing capacity in operations](#analyzing-capacity-in-operations)
  - [Some examples of capacity in organizations](#some-examples-of-capacity-in-organizations)
  - [Importance of Capacity](#importance-of-capacity)
  - [Definition of Capacity](#definition-of-capacity)
  - [Measuring Capacity](#measuring-capacity)
  - [Capacity Planning Issues](#capacity-planning-issues)
    - [A Healthcare example](#a-healthcare-example)
  - [Process Analysis](#process-analysis)
    - [Building Blocks](#building-blocks)

A Systematic approach to understanding and address issues about the operations system.

An operations system is defined as one which

- Several activities are performed
- To transform a set of inputs into useful output
- using a transformation process
- involving multiple stakeholders

# Agenda

- Describe the overall structure of operations in organizations and how it is linked to other functional areas.
- Understand some broad patterns in which the Operations are generally organized and their implications to managing operations.
- Summarize the factors that drive the complexity of Operations Management.
- Explain the need for performance measures and how organizations can select appropriate performance measures.

> First step is to understand the operations, and set of issues that must be addressed.

# Why operations management?

- It ensures sustainability, consistency, and a way to measure operational efficiency.

# Role of Operations Management

![operations-management single stage](./assets/operation-management.svg)

- Do we have the correct capacity for demand?
- Do we have the right resources?
- Do we have a quality assurance system?
- How do we improve the productivity of processes and people?
- How do we address supply chain issues?

# Alternative Configurations in Operations Management

## Streamlined/Continous Flow Systems

![Streamlined flow example](./assets/streamlined-flow-example.PNG)

> Typical Characteristic: Low variety/High volumes

### Issues

- Ensure continuous flow
- Balance work or capacity
- Maintenance Management
  - Is defined as the process of maintaining a company's assets and resources while controlling time and costs, ensuring maximum efficiency of the manufacturing process.
- Ensure a quality assurance system

## Intermittent Flow Systems

### Features

- Characterized by mid-volume, mid-variety products/services
- Increases the flow complexities
- Flow and capacity balancing are difficult but important
  - Batch Processing
  - Alternative methods of work organization
- Capacity Estimation is hard
- Production Planning & Control is complex

Examples:

- Travel agency: Adventure tourism, eco/nature tourism, personal vacations, etc.
- Number of tariffs and plans offered by mobile telephone providers or satellite-based DTH entertainment services is on the increase
- Product variations: Do it yourself products and services

### Sources of Problems

![Source of problems in intermittent flow](./assets/intermittent-flow-problems.PNG)

- Inappropriate Choice on Structure & People Issues
- Complex Material & Information Flows
- Complex Operations Planning & Control

### Role of Operations Management

- Manage the offering
  - Variety Management
  - modular designs
  - delayed differentiation
- Design an appropriate Operating System
  - Divide & Rule
- Manage operations
  - Changeover from one mode to another

## Jumbled Flow Systems

![Jumbled process flow](./assets/jumbled-process-flow.PNG)

### Issues to address

- Non-standard, complex flow patterns (Highly customized items, customer orders for one or a few)
- Very High Variety, Low volumes
- No benefits arising out of volume or scale of operations
- Large uncertainty, Too many entities involved
- Longer period
- Difficult to dedicate resources exclusively for requirements
  - Sharing of common resources is a reality

### How to handle this?

- Operations Planning and Control too complex
- Needs sub-system oriented thinking
- Taking uncertainty into consideration is important
- Project Management tools and techniques are critical

## Observations

### Volume Variety

![Making sense of operations](./assets/making-sense-of-operations.PNG)

- Volume & Variety always a trade-off with one another
- There is a trend towards the mid-volume mid-variety situation
- Flow is always related to the volume–variety interactions
  - High Volume–Low Variety: Continuous Flow
  - Low Volume–High Variety: Jumbled Flow
  - Mid Volume–Mid Variety: Intermittent Flow

### Complexity

![Complexity in operations management](./assets/complexity-in-operations-management.PNG)

- The Notion of variety is to be understood
  - Products, Models, Processes, Routing, Technology Choices
- Factors Affecting Operations Management Complexity
  - Volume–Variety Interactions leading to flow
  - Number of Stages in Operations

# Perfomance metrics

![Performance metric comparison](./assets/performance-metrics-analysis.PNG)

Inference

- Operational choices will eventually affect the performance of an organization.
- Performance Metrics help us assess the impact of these choices.

## Performance Metrics for Operations

- Quality –PPM, DPMO, Quality Costs, FPY
- Cost –Inventory (days), Procurement, Production
- Delivery –Order Fulfillment time, OTD Index, Schedule Adherence
- Flexibility –No. of models,
- Responsiveness –Waiting Time, Delivery Quote
- Innovation –No. of new models, Patents,
- Learning –Training time, Suggestions per employee
- Improvement –NVA Content

  ### Growing customer expectations

  - Customers tend to demand more and refine their expectations.
  - Manufacturing & Service organizations must learn to respond to these expectations.
  - Need to develop capabilities to bring newer products and services faster and yet profitably.

- Today’s businesses are constantly challenged by the rapid technological advancements.
  - ATMs & Internet Banking, Procurement of goods & services, New Product Development
- Environmental Issues–Growing industrialization raises concerns regarding the depletion of natural resources and the waste generated from production systems and end-of-life products.
  - Increasingly, firms are under pressure to take responsibility for restoring, sustaining, and expanding the planet’s ecosystem instead of merely exploiting it.

### Priorities for Operations Today

- Relate operations system to Customer/ Market.
- Acquire Capabilities to tolerate product proliferation.
- Develop systems and procedures that promote learning.
- Develop Green Manufacturing Practices.

### Order winning and Order qualifying attributes

- Order qualifying attributes are the set of attributes that customers expect in the product or service they consider for buying.
- Order winning attributes are other attributes that have the potential to sufficiently motivate the customer to buy the product or service.

# Analyzing capacity in operations

## Some examples of capacity in organizations

- Associated Cements Corporation (ACC) has an installed capacity of 17.65 million tons.
- Bharat Petroleum Corporation Limited (BPCL) has a refining capacity of 260,000 barrels of crude every day.
- Recently Tata Consultancy Services (TCS), India's leading software firm announced that it will double its capacity at its largest delivery center, Hyderabad, with an addition of 28,000 employees over a period of three to four years.

## Importance of Capacity

Capacity and its relationship to competitiveness of business firms.

- In the Executive health check-up unit in a multi-specialty hospital many people wait in the system for their turn and some may choose to post-pone or abandon the idea.
- Firms find it difficult to deliver products and services on the promised date simply because of a huge backlog in customer orders.
- In a manufacturing shop floor components pile up in front of some machines while some other machines downstream starve for want of components for processing.

When capacity choices have not been made appropriately it can result in:

- Market share loss
- Loss of goodwill
- Customer dissatisfaction etc.

## Definition of Capacity

Capacity directly relates to the quantum of these resources that an operating system has:

- In the case of a garment manufacturer the number of cutting and sewing machines, the number of people employed in the shop for production, quality assurance etc. will all determine the capacity of the system.
- In a restaurant the number of dining spaces, the capacity of various machines used for cooking, the number of servers and chefs, availability of utensils, plates, cups etc. will all determine the capacity in the system.
- It also denotes the maximum output of products and services one can achieve using these resources.

## Measuring Capacity

Capacity is measured in two ways in any organization:

- On the basis of output: An automobile manufacturer such as Toyota will measure capacity on the basis of daily production of vehicles, say 20,000 vehicles per day.
- In terms of input resources: A software service provider or a management consulting firm can measure capacity in terms of the number of professionals that they have.

## Capacity Planning Issues

What is of interest to us are the following:

- How do these multiple resources in an operating system eventually determine the capacity of the system?
- How do they affect the outcomes and customer oriented measures such as output, and delivery promises?
- How to make an assessment of the capacity in my organization?
- What kind of data are required in order to estimate the capacity in an organization?

### A Healthcare example

Issues affecting capacity in a typical hospital:

- Doctors, para-medical staff who are involved in the treatment of the patients.
- The hospital uses certain equipments, diagnostic gadgets such as scanners.
- The hospital may also have a certain number of waiting spaces, operating theaters etc.

Given a certain number of each of these resources,

- How it will affect the outcome in terms patient waiting, residence time in the hospital?
- These are determined by the design of the process deployed to deliver health care.

## Process Analysis

Process Analysis is a method by which:

- We can incorporate details pertaining to resources and other process related data.
- Use some logic to analyze these aspects pertaining to the capacity in the system.

### Building Blocks

- Activities: These are the building blocks that make up a process
  - let us look at one manufacturing and one service example to understand this.
- Technological & Logical Constraints: Dictates the order in which the steps are carried out. This is important because only then we will know the flow of activities.

### Activities & Process Constraints:

The Case of Shirt Manufacturing
